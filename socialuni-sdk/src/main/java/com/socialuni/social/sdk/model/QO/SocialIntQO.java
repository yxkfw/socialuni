package com.socialuni.social.sdk.model.QO;

import lombok.Data;

@Data
public class SocialIntQO {
    private Integer number;
}