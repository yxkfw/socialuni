package com.socialuni.social.sdk.constant;

public class SocialType {
    // 交友
    public static final String dating = "dating";
    // 校园
    public static final String school = "school";
}