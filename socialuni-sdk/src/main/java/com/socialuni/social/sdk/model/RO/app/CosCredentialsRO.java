package com.socialuni.social.sdk.model.RO.app;

import lombok.Data;

@Data
public class CosCredentialsRO {
    String tmpSecretId;
    String tmpSecretKey;
    String sessionToken;
}
