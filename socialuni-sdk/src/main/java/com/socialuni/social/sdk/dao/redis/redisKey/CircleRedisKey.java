package com.socialuni.social.sdk.dao.redis.redisKey;

/**
 * redisTemplate封装
 *
 */
public class CircleRedisKey {
    //系统
    public static final String tagById = "circleById";
    public static final String tagByName = "circleByName";
    public static final String tagByDevId = "circleByDevId";

    public static final String talkTagsByTalkId = "talkCirclesByTalkId";

}