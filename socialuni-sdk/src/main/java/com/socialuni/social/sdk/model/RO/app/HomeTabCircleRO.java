package com.socialuni.social.sdk.model.RO.app;

import com.socialuni.social.sdk.model.RO.community.circle.SocialCircleRO;
import lombok.Data;

/**
 * @author qinkaiyuan
 * @date 2020-05-23 17:21
 */
@Data
public class HomeTabCircleRO {
    private String name;
    private SocialCircleRO circle;
}
